### **RelayBox**

API client

**Installation**

Via composer

If composer is set global     
```
composer require relaybox/relaybox "^1.0.0"
```

Or if composer is set to the local directory     
```
php composer.phar require relaybox/relaybox "^1.0.0"
```

**Requirements**

- php >= 5.4
- curl

**Usage**

```
$relaybox = new RelayBox\RelayBox($your_public_id, $your_private_key);

$data = [     
      'message'  => 'Awesome message here!',     
      'options'  => [     
          'public' => 0,     
      ],     
    ];     

$array = $relaybox->send($data);

print_r($array);
```