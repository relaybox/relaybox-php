<?php

/**
 * 
 * relaybox.io api client
 * https://bitbucket.org/relaybox/relaybox-php
 * 
 */


namespace RelayBox;

class RelayBox {
    
    const URL_BASE = 'https://api.relaybox.io/';
    const VERSION = '1.0.2';
    private $data = [];
    private $public;
    private $private;
    
    function __construct($public_id, $private_key) {
        $this->public = $public_id;
        $this->private = $private_key;
    }

    public function send(array $data){
        $this->data = $data;
        $path = 'message/send';
        return $this->fire($path, $data);
    }
    
    public function getRecent(){
        $path = 'message/get';
        return $this->fire($path);
    }
    
    public function getMessage($message_id){
        $path = 'message/get/' . $message_id;
        return $this->fire($path);
    }
    
    private function fire($path, array $data = []){

        $data['time'] = time();    

        $json = json_encode( $data );
        $hash = hash_hmac('sha256', $json, $this->private);

        $curl = curl_init(self::URL_BASE . $path);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
                     ["Content-type: application/json",
                      "X-APP-PUB: " . $this->public,
                      "X-APP-HASH: "  . $hash,
                      "X-APP-API-VERSION: " . self::VERSION,]);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $json);

        $json_response = curl_exec($curl);

        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ( $status == 200 ) {
            $return = $response = json_decode($json_response, true);
        } else {
            $error_array = [
                'failed' => $path,
                'status' => $status,
                'response' => $json_response,
                'curl_error' => curl_error($curl),
                'curl_errno' => curl_errno($curl),
                ];
            $return = $error_array;
        }

        curl_close($curl);

        return $return;
    }  
    
}